"use strict";


function deleteRow(event){
    let btn = event.target;
    btn.parentElement.parentElement.remove();
}

let btnDeleteList = document.getElementsByClassName("delete-row");
for (let i = 0; i < btnDeleteList.length; i++) {
    btnDeleteList[i].onclick = deleteRow;
}

function addRow(model,prix,marque,plaque,annee){
    let row = document.createElement("tr");

    let td_model = document.createElement("td");
    td_model.textContent = model;
    row.appendChild(td_model);

    let td_prix = document.createElement("td");
    td_prix.textContent = prix;
    row.appendChild(td_prix);

    let td_marque = document.createElement("td");
    td_marque.textContent = marque;
    row.appendChild(td_marque);

    let td_plaque = document.createElement("td");
    td_plaque.textContent = plaque;
    row.appendChild(td_plaque);

    let td_annee = document.createElement("td");
    td_annee.textContent = annee;
    row.appendChild(td_annee);


    let td_button = document.createElement("td");

    let button = document.createElement("button");
    button.className = "btn btn-danger mt-2 mb-2";
    button.onclick = deleteRow;
    button.textContent = "Supprimer"
    td_button.appendChild(button);
    row.appendChild(td_button)

    let tbody = document.getElementsByTagName("tbody").item(0);
    tbody.appendChild(row);
}


let form = document.forms.addcar;


function addingCar(event){
    event.preventDefault();

    addRow(form.modele.value,
        form.prix.value,
        form.marque.value,
        form.plaque.value,
        form.annee.value); //ajoute la ligne de html

    form.reset(); //supprime les donénes



    //fermer la modal
    let modal = document.getElementById("addCarModal");
    modal.classList.remove('show');
    modal.setAttribute('aria-hidden', 'true');
    modal.setAttribute('style', 'display: none');

    const modalBackdrops = document.getElementsByClassName('modal-backdrop');
    document.body.removeChild(modalBackdrops[0]);

}

form.onsubmit = addingCar;

