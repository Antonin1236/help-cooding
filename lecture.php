<?php

    try{
        $article = fopen('article.txt', 'rt'); //On ouvre le fichier

        #Tant que il reste des lignes dans le fichier
        while (($line = fgets($article)) !== false) {

            // On supprime les caractères qui deviennent inutile à cause des retour à la ligne
            
            $line_clean = str_replace(['(', ')', '.', ','], "", $line);

            //On sépare la ligne au niveau des " " (espaces)  et des ' (on fait donc une list de mot)
            $word_arr = preg_split('/[\s]|[\’]|[-]/', $line_clean);

            //On parcour la list de mot et on les affiches avec un retour à la ligne
            foreach($word_arr as $word){
                if ($word != ""){
                    echo $word."<br/>"; 
                }

            }
        }

        //on ferme le fichier
        fclose($article);

    }catch(Exception $e){
        echo "Erreur lors de l'ouverture : " . $e->getMessage() ;
    }
    


?>