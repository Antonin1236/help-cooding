<?php
    $listeNbPersonneParTable = Array(4, 4, 4, 4, 2, 2);

    $assiettes=26;
    $couteaux=26;
    $cuillers=26;
    $cuillersDessert=26;

    foreach ($listeNbPersonneParTable as $nbPersonne){
        $assiettes          -=  $nbPersonne;
        $couteaux           -=  $nbPersonne;
        $cuillers           -=  $nbPersonne;
        $cuillersDessert    -=  $nbPersonne;

        echo "Après avoir mis en place cette table de ".$nbPersonne." personnes. Il reste : \n";
        echo $assiettes." assiettes\n";
        echo $couteaux." couteaux\n";
        echo $cuillers." cuillers\n";
        echo $cuillersDessert." cuillers de desserts\n";

    }

    echo "A la fin de la mise en place des tables il reste : \n";
    echo $assiettes." assiettes\n";
    echo $couteaux." couteaux\n";
    echo $cuillers." cuillers\n";
    echo $cuillersDessert." cuillers de desserts\n";
?>
