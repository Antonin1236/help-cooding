<?php

require_once __DIR__ . '/utils/Router.php';
require_once __DIR__ . '/controllers/Home.php';
require_once __DIR__ . '/controllers/API.php';
require_once   __DIR__ . '/utils/File.php';

// Default index page
router('GET', '^/$',  function() {
    Home::getHome();
});

// GET request to /users
router('POST', '^/add_car$', function ($params){
    $json = json_decode(file_get_contents('php://input'), true);
    API::add_car($json);
});

router('GET', '^/delete_car/(?<id>\d+)$',  function($params) {
    API::remove_car ($params);
});

header("HTTP/1.0 404 Not Found");
echo '404 Not Found';

