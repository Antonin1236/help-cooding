<?php

    require_once   __DIR__ . '/../models/Voiture.php';
    class API{
        public static function add_car($request){

            //test si les champs sont bien remplient
            if(isset( $request["modele"])&&
                isset( $request["prix"])&&
                isset( $request["marque"])&&
                isset( $request["plaque"])&&
                isset( $request["annee"])){

                //on test si les champs sont du bon type
                if(is_numeric($request["prix"]) &&
                   is_numeric($request["annee"])){



                    $v = new voiture(
                        htmlentities($request["modele"]), //htmlentities pour eviter les injections sql
                        (int)($request["prix"]),
                        htmlentities($request["marque"]),
                        htmlentities($request["plaque"]),
                        (int)($request["annee"])
                    );

                    $v->save();

                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode(array("id"=>$v->id));


                }else{
                    header("HTTP/1.0 400 Type fields not valid");
                }

            }else{

                header("HTTP/1.0 400 Empty fields");
            }

        }


        public static function remove_car($request){
            Voiture::remove($request['id']);

        }
    }
