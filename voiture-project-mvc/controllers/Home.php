<?php
    require_once   __DIR__ . '/../models/Voiture.php';

    class Home{
        public static function getHome(){
            $title = "Site de voiture"; //titre de la page

            $Voitures= Voiture::getAll();


            include(File::build_path(array("views", "head.php"))); //head html avec les meta
            include(File::build_path(array("views", "home.php"))); // contenue du body
            include(File::build_path(array("views", "end.php"))); //footer de la page
        }

    }