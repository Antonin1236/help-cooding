<?php

    require_once   __DIR__ . '/VoitureDao.php';

    class Voiture{
        public $modele;
        public $prix;
        public $marque;
        public $id = null;
        public $plaque;
        public $annee;

        public function __construct($modele, $prix,$marque,$plaque,$annee, $id=null)
        {
            $this->modele = $modele;
            $this->prix = $prix;
            $this->marque = $marque;
            $this->plaque = $plaque;
            $this->annee = $annee;
            $this->id = $id;
        }

        public static function getAll(){
            $list = [];
            $array = VoitureDao::getAllVoiture();
            foreach($array as $obj){
                array_push($list,
                    new Voiture($obj["modele"],
                    $obj["prix"],
                    $obj["marque"],
                    $obj["plaque"],
                    $obj["annee"],
                    $obj["id"]));
            }
            return $list;
        }

        public static function remove($id){
            VoitureDao::removeVoiture($id);
        }


        public  function  save(){
            $this->id = VoitureDao::insertVoiture(
                $this->modele,
                $this->prix,
                $this->marque,
                $this->plaque,
                $this->annee
            )["MAX(id)"];
        }

        public static function get($id){
            $obj = VoitureDao::getVoiture($id);
            return new Voiture(
                $obj["modele"],
                $obj["prix"],
                $obj["marque"],
                $obj["plaque"],
                $obj["annee"],
                $obj["id"]
            );
        }

    }