<?php
    require_once   __DIR__ . '/../utils/Database.php';

    class VoitureDao{

        public static function insertVoiture($modele,$prix,$marque, $plaque, $annee){
            try {
                $req = Database::getPDO()->prepare("INSERT INTO voiture(id,modele, prix, marque, plaque, annee) VALUES(NULL , :modele, :prix, :marque, :plaque, :annee)");
                $req->execute(array(
                    'modele' => $modele,
                    'prix' => $prix,
                    'marque' => $marque,
                    'plaque' => $plaque,
                    'annee' => $annee
                ));


                return Database::getPDO()->query("SELECT MAX(id) FROM voiture")->fetch();

            }catch(PDOException $e) {
                echo "Insert failed: " . $e->getMessage();
            }

        }
        public static function getVoiture($id){
            return Database::getPDO()->query("SELECT * FROM voiture where id = '$id'")->fetch();
        }

        public static function removeVoiture($id){
            Database::getPDO()->query("DELETE FROM voiture WHERE id = '$id'");
        }
        public static function getAllVoiture(){

            return Database::getPDO()->query("SELECT * FROM voiture")->fetchAll();

        }
    }