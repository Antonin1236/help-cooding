<div class="container bg-white b-5 mt-5">
    <h1>Catalogue de voitures !</h1>
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addCarModal">
        Ajouter une voiture
    </button>

    <table class="table table-striped table-hover">
        <thead class="thead-dark">
        <tr class="text-center">
             <th>modele</th>
             <th>prix</th>
             <th>marque</th>
             <th>plaque</th>
             <th>annee</th>
             <th>action</th>
        </tr>
        </thead>
        <tbody id="VoitureList" class="text-center">
        <?php
        foreach($Voitures as $v){
            echo '<tr id="'.$v->id.'">';

                echo '<td>'.$v->modele.'</td>';
                echo '<td>'.$v->prix.'</td>';
                echo '<td>'.$v->marque.'</td>';
                echo '<td>'.$v->plaque.'</td>';
                echo '<td>'.$v->annee.'</td>';
            echo '<td><button class="btn btn-danger mt-2 mb-2 delete-row">Supprimer</button></td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</div>
<footer class="container fixed-bottom bg-dark text-light">
</footer>

<!-- Modals -->
<div class="modal fade" id="addCarModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Formualaire d'ajout d'une voiture</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form name="addcar">
                    <fieldset>
                        <div class="form-group">
                            <label for="modele">Entrez un modèle de voiture</label>
                            <input type="text" name="modele" class="form-control" id="modele" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label for="prix">Entrez un prix</label>
                            <input type="number" name="prix" class="form-control" id="prix" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label for="marque">Entrez une marque de voiture</label>
                            <input type="text" name="marque" class="form-control" id="marque" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label for="plaque">Entrez une plaque d'immatriculation</label>
                            <input type="text" name="plaque" class="form-control" id="plaque" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label for="annee">Entrez l'année</label>
                            <input type="number" name="annee" class="form-control" id="annee" placeholder="" required>
                        </div>
                    </fieldset>

                    <button type="submit" class="btn btn-primary">Ajouter la voiture</button>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>