create table voiture
(
    id     INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    modele VARCHAR(100),
    prix   INT,
    marque VARCHAR(255),
    plaque VARCHAR(100),
    annee  INT
);

--CREATE USER 'voiture'@'localhost' IDENTIFIED BY 'Voiture111&efsq';