<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>Catalogue de voitures</title>
    <link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet">
</head>
<body class="bg-light">
    <div class="container bg-white b-5 mt-5">
        <h1>Catalogue de voitures !</h1>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addCarModal">
          Ajouter une voiture
        </button>

        <table class="table table-striped table-hover">
            <thead class="thead-dark">
                <tr class="text-center">
                <?php
                  $Voitures= [
                    ['modele' => 'Clio','prix' => 1000,'marque'=>'Renault','plaque'=>'479-566-LB','annee'=>1998],
                    ['modele' => 'Clio','prix' => 1000,'marque'=>'Renault','plaque'=>'479-566-LB','annee'=>1998],
                    ['modele' => 'Clio','prix' => 1000,'marque'=>'Renault','plaque'=>'479-566-LB','annee'=>1998],
                    ['modele' => 'Clio','prix' => 1000,'marque'=>'Renault','plaque'=>'479-566-LB','annee'=>1998],
                    ['modele' => 'Clio','prix' => 1000,'marque'=>'Renault','plaque'=>'479-566-LB','annee'=>1998],
                  ];
                    echo '<th>modele</th>';
                    echo '<th>prix</th>';
                    echo '<th>marque</th>';
                    echo '<th>plaque</th>';
                    echo '<th>annee</th>';
                    echo '<th>action</th>';
                ?>
                </tr>
            </thead>
            <tbody id="VoitureList" class="text-center">
              <?php
              foreach($Voitures as $v => $infos){
                echo '<tr>';
                foreach ($infos as $c => $v){
                    echo '<td>'.$v.'</td>';
                }
                echo '<td><button class="btn btn-danger mt-2 mb-2 delete-row">Supprimer</button></td>';
                echo '</tr>';
              }
              ?>
            </tbody>
        </table>
    </div>
    <footer class="container fixed-bottom bg-dark text-light">
    </footer>

    <!-- Modals -->
    <div class="modal fade" id="addCarModal" tabindex="-1" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Formualaire d'ajout d'une voiture</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <form name="addcar">
                  <fieldset>
                    <div class="form-group">
                      <label for="modele">Entrez un modèle de voiture</label>
                      <input type="text" name="modele" class="form-control" id="modele" placeholder="" required>
                    </div>
                    <div class="form-group">
                      <label for="prix">Entrez un prix</label>
                      <input type="text" name="prix" class="form-control" id="prix" placeholder="" required>
                    </div>
                    <div class="form-group">
                      <label for="marque">Entrez une marque de voiture</label>
                      <input type="text" name="marque" class="form-control" id="marque" placeholder="" required>
                    </div>
                    <div class="form-group">
                      <label for="plaque">Entrez une plaque d'immatriculation</label>
                      <input type="text" name="plaque" class="form-control" id="plaque" placeholder="" required>
                    </div>
                    <div class="form-group">
                      <label for="annee">Entrez l'année</label>
                      <input type="text" name="annee" class="form-control" id="annee" placeholder="" required>
                    </div>
                  </fieldset>

                  <button type="submit" class="btn btn-primary">Ajouter la voiture</button>
                </form>
          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>


    <!--- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="app.js"></script>
</body>

</html>
